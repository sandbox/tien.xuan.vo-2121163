<?php

/**
 * @file
 * Definition of snhs_handler_filter_term_node_tid.
 */

/**
 * Filter by term id (including selection by simple non-hierarchical select).
 *
 * @ingroup views_filter_handlers
 */
class snhs_handler_filter_term_node_tid_depth extends views_handler_filter_term_node_tid {

  /**
   * {@inheritdoc}
   */
  public function exposed_validate(&$form, &$form_state) {
    parent::exposed_validate($form, $form_state);
  }

  /**
   * Convert structured terms by level into flat array of terms and then apply
   * where query with correct operator.
   */
  public function query() {
    $level = 0;
    $hierarchical_terms = array();
    $values = $this->value;

    while ($values) {
      if (isset($values["lvl$level"]) && !empty($values["lvl$level"])) {
        // This level has terms.
        $terms = explode(':', $values["lvl$level"]);
        if (empty($hierarchical_terms)) {
          // First terms in hierarchical tree.
          $hierarchical_terms["lvl$level"] = $terms;
        }
        else {
          // Get valid terms.
          $parent_level = $level - 1;
          $valid_children_terms = array();
          foreach ($hierarchical_terms["lvl$parent_level"] as $parent_term) {
            $valid_children_terms = array_merge(
              $valid_children_terms,
              array_keys(taxonomy_get_children($parent_term))
            );
          }
          $hierarchical_terms["lvl$level"] = array_intersect(
            $valid_children_terms,
            $terms
          );
          // Remove all parents.
          for ($i = 0; $i < $level; $i++) {
            if (isset($hierarchical_terms["lvl$i"])) {
              unset($hierarchical_terms["lvl$i"]);
            }
          }
        }
      }
      elseif ($level > 0) {
        // If this level is empty, get children of all parents.
        $parent_level = $level - 1;
        if (isset($hierarchical_terms["lvl$parent_level"]) &&
          !empty($hierarchical_terms["lvl$parent_level"])) {
          foreach ($hierarchical_terms["lvl$parent_level"] as $parent_term) {
            if (isset($hierarchical_terms["lvl$level"])) {
              $hierarchical_terms["lvl$level"] = array_merge(
                $hierarchical_terms["lvl$level"],
                array_keys(taxonomy_get_children($parent_term))
              );
            }
            else {
              $hierarchical_terms["lvl$level"] =
                array_keys(taxonomy_get_children($parent_term));
            }
          }
        }
      }
      unset($values["lvl$level"]);
      $level++;
    }

    // Prepare value.
    if (empty($hierarchical_terms)) {
      // Get all values.
      $this->value = NULL;
    }
    else {
      // Get values by terms.
      $this->value = array();
      foreach ($hierarchical_terms as $terms) {
        $this->value = array_merge($this->value, $terms);
      }
    }

    // Get the correct operator.
    if (count($this->value) == 0) {
      // If no filter values are present, then do nothing.
      return;
    }
    elseif (count($this->value) == 1) {
      // $this->value is an array with a single element so convert it.
      if (is_array($this->value)) {
        $this->value = current($this->value);
      }
      $operator = '=';
    }
    else {
      $operator = 'IN';
    }

    // The normal use of ensure_my_table() here breaks Views.
    // So instead we trick the filter into using the alias of the base table.
    // See http://drupal.org/node/271833
    // If a relationship is set, we must use the alias it provides.
    if (!empty($this->relationship)) {
      $this->table_alias = $this->relationship;
    }
    // If no relationship, then use the alias of the base table.
    elseif (isset($this->query->table_queue[$this->query->base_table]['alias'])) {
      $this->table_alias = $this->query->table_queue[$this->query->base_table]['alias'];
    }
    // This should never happen, but if it does, we fail quietly.
    else {
      return;
    }

    // Now build the subqueries.
    $subquery = db_select('taxonomy_index', 'tn');
    $subquery->addField('tn', 'nid');
    $where = db_or()->condition('tn.tid', $this->value, $operator);

    $subquery->condition($where);
    $this->query->add_where($this->options['group'], "$this->table_alias.$this->real_field", $subquery, 'IN');
  }

  /**
   * Add new selection type option definition.
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['type'] = array('default' => 'snhs');

    return $options;
  }

  /**
   * Add new selection type option.
   */
  public function extra_options_form(&$form, &$form_state) {
    parent::extra_options_form($form, $form_state);

    $form['type']['#options'] += array(
      'snhs' => t('Simple non-hierarchical select'),
    );
  }

  /**
   * Create simple non-hierarchical select form,
   * Parent and children dropdown are showed in the same time.
   */
  public function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);

    $vocabulary = taxonomy_vocabulary_machine_name_load($this->options['vocabulary']);
    if (empty($vocabulary) && $this->options['limit']) {
      $form['markup'] = array(
        '#markup' => '<div class="form-item">' . t('An invalid vocabulary is selected. Please change it in the options.') . '</div>',
      );
      return;
    }

    if ($this->options['type'] == 'snhs' && $this->options['exposed']) {
      $terms = taxonomy_get_tree($vocabulary->vid);
      $level = 0;
      $non_hierarchical_terms = array();

      while ($terms) {
        foreach ($terms as $key => $term) {
          if ($level == 0 && $term->parents[0] == 0) {
            // Get root terms.
            $non_hierarchical_terms[$level][$term->tid] = $term->name;
            unset($terms[$key]);
          }
          elseif ($level >= 1) {
            foreach ($non_hierarchical_terms[$level - 1] as $parent_id => $parent_term) {
              // Get children terms.
              if ($term->parents[0] == $parent_id) {
                if (!isset($non_hierarchical_terms[$level]) ||
                  empty($non_hierarchical_terms[$level]) ||
                  !in_array($term->name, $non_hierarchical_terms[$level])) {
                  // This level is empty, or this term's name is new in this level.
                  $non_hierarchical_terms[$level][$term->tid] = $term->name;
                  unset($terms[$key]);
                }
                else {
                  // This term's name has already existed in this level.
                  $sub_key = array_search($term->name, $non_hierarchical_terms[$level]);
                  if ($sub_key) {
                    // Merge terms in this level.
                    unset($non_hierarchical_terms[$level][$sub_key]);
                    $non_hierarchical_terms[$level][$sub_key . ':' . $term->tid] = $term->name;
                    unset($terms[$key]);
                  }
                  else {
                    // Never happened.
                  }
                }
              }
            }
          }
        }
        $level++;
      }

      $form['value'] = array(
        '#type' => 'fieldset',
        '#title' => 'Simple Non-hierarchical Select',
        '#tree' => TRUE,  // Important, this make exposed_validate() pass.
      );
      for ($i = 0; $i < $level; $i++) {
        $form['value']["lvl$i"] = array(
          '#type' => 'select',
          '#options' => $non_hierarchical_terms[$i],
          '#empty_value' => ''
        );
      }
    }

    if (empty($form_state['exposed'])) {
      // Retain the helper option.
      $this->helper->options_form($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function admin_summary() {
    $this->value = NULL;

    return parent::admin_summary();
  }

  /**
   * Disable multiple checkbox.
   */
  public function expose_form(&$form, &$form_state) {
    parent::expose_form($form, $form_state);
    // This filter doesn't support multiple values.
    unset($form['expose']['multiple']);
  }

}
