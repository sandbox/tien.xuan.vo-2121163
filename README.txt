
-- SUMMARY --

This module creates a simple non-hierarchical select views filter,
with all repeated children are merged.
This module is based on https://drupal.org/project/shs module.


-- REQUIREMENTS --

Assume that you have a vocabulary named 'services' and has structure like this:
-Paris (1)
---Restaurant (2)
---Hotel (3)
-New York (4)
---Restaurant (5)
---Hotel (6)
-Other location (7)
---Restaurant (8)
---Hotel (9)

Suppose you can't change this structure (e.g. separate this vocabulary
into 2 vocabularies, make a term child of multiple parents...)

What if you want to find all services in Paris?
What if you want is find all Restaurant services?

This module provide views filter like shs module does, but it shows parent
and children dropdown at the same time, so you can select 'Restaurant' in the
child dropdown to find all restaurant services, or select 'Paris' in the first
dropdown to find all services in Paris, or specify 2 dropdowns to find all
restaurants in Paris.


-- REQUIREMENTS --

This modules requre these modules to be enabled:
* Taxonomy (Drupal core)
* Views

There are some known bugs of views 3.7 about 'Grouped Exposed taxonomy term
filters', you can solve them by apply these patches:
* https://drupal.org/node/2049603#comment-7923443
* https://drupal.org/node/1810148#comment-6961380


-- INSTALLATION --

* Install as usual, see
  http://drupal.org/documentation/install/modules-themes/modules-7 for further
  information.


-- CONFIGURATION --

* Views (exposed filter)
  - add a new filter "Content: Has taxonomy terms (with depth; Simple
      non-hierarchical select)"
  - use "Simple non-hierarchical select" as selection type
  - select "Expose this filter to visitors, to allow them to change it"
  - enjoy :)

-- CONTACT --

Current maintainers:
* Vo Xuan Tien (tien.xuan.vo) - http://drupal.org/user/2535842

This project has been sponsored by:
* go1
  One of Australia's premier Drupal web design and development agencies.
  Visit http://www.go1.com.au for more information.
